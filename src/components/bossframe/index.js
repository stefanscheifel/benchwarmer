import React, { Component, Fragment } from "react";

export default class Bossframe extends Component {
  render() {
    const { increment, decrement, count } = this.props;
    console.log(this.props);

    return (
      <Fragment>
        <button onClick={increment}>inc</button>
        <button onClick={decrement}>dec</button>
        <h1 style={{ color: "white" }}>{count}</h1>
      </Fragment>
    );
  }
}
