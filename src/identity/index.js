import Theme from "./theme.js";
import * as Colors from "./colors.js";

export default { Theme, Colors };
