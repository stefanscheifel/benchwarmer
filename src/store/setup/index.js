import { createStore } from "redux";
import { combineReducers } from "redux";
import roster from "../roster/reducer";

const rootReducer = combineReducers({});

const defaultState = { counter: 10 };

const store = createStore(
  roster,
  defaultState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

console.log(store);

export default store;
