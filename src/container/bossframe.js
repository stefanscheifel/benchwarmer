import { connect } from "react-redux";

import Bossframe from "../components/bossframe";
import { increment, decrement } from "../store/roster/actions";
import { selectCounter } from "../store/roster/selector";

const mapStateToProps = state => ({
  count: selectCounter(state)
});

const mapDispatchToProps = dispatch => ({
  increment: () => dispatch(increment()),
  decrement: () => dispatch(decrement())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bossframe);
