import React, { Fragment } from "react";
import styled from "styled-components";
import { Box, Flex } from "rebass";
import Identity from "../identity";
import Bossframe from "../container/bossframe";
export default () => (
  <Flex>
    <Fragment>
      <Box p={3} width={1 / 3} color={Identity.Colors.greytext} bg="black">
        <Bossframe />
      </Box>
      <Box p={3} width={2 / 3} color={Identity.Colors.greytext} bg="#F46F5A">
        Boss
      </Box>
    </Fragment>
  </Flex>
);
